<?php

return [
    'guard' => 'user',
    'storages' => [
        [
            'disk_name' => 'file-manager-public',
            'folder_name' => 'Public Folder',
            'folder_feather' => 'folder',
            'default' => true,
            'role' => 'public',
            'permissions' => [
                'upload' => true,
                'create_folder' => true,
                'delete' => true,
                'rename' => true,
                'copy' => true,
                'cut' => true
            ]
        ],
        /*
        [
            'disk_name' => 'file-manager-private',
            'folder_name' => 'Private Folder',
            'folder_feather' => 'folder',
            'role' => 'private',
            'permissions' => [
                'upload' => true,
                'create_folder' => true,
                'delete' => true,
                'rename' => true,
                'copy' => true,
                'cut' => true
            ]
        ],
        [
            'disk_name' => 'file-manager-cache',
            'folder_name' => 'Cache',
            'folder_feather' => 'hash',
            'role' => 'cache',
            'permissions' => [
                'upload' => false,
                'create_folder' => false,
                'delete' => false,
                'rename' => false,
                'copy' => false,
                'cut' => false
            ]
        ],
        [
            'disk_name' => 'file-manager-trash',
            'folder_name' => 'Trash',
            'folder_feather' => 'trash-2',
            'role' => 'trash',
            'permissions' => [
                'upload' => false,
                'create_folder' => false,
                'delete' => false,
                'rename' => false,
                'copy' => false,
                'cut' => false
            ]
        ],
        */
    ],
    'events' => [
        'changeFolderPath' => [
            // [\App\Models\Model::class => 'method_name']
        ],
        'changeFilePath' => [
            // [\App\Models\Model::class => 'method_name']
        ],
        'checkUsedImage' => [
            // [\App\Models\Model::class => 'method_name']
        ]
    ]
];
