# Redlab / FileManager

## Обязательные NPM пакеты

| Packet                 | NPM command                                      | 
|------------------------|--------------------------------------------------|
| feather-icons          | npm i feather-icons                              | 
| vue2-perfect-scrollbar | npm i vue2-perfect-scrollbar                     | 

## Установка
1. Добавить в composer.json (добавляет git url для поиска нашего пакета)
   
    ```
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/redlab_ua/redlab.filemanager.git"
        }
    ]
    ```

2. Прописать команду установки пакета
    ```  
    composer require redlab/filemanager
    ```
3. Установить пакеты из обязательного списка(если еще не установлены)


4. Прописать подключение файла app.js из модуля

    ```
    require('./../../vendor/redlab/filemanager/resources/assets/app.js');
    ```
   
5. Прописать команду 

    ```
    php artisan vendor:publish
    ``` 
    Выбрав из списка 
    ```
    Provider: FileManager\FileManagerServiceProvider
    ```
    или вручную скопировать конфиг пакета
    ```
    [\vendor\redlab\filemanager\configs\file-manager.php] To [\config\file-manager.php]
    ```
   
6. Настроить конфиг и storage
   

7. После выполненных действий, будет глобально доступен Vue тег 
    ```
    <file-manager></file-manager>
    ```

## Laravel Events
События, на которые может подписаться Laravel из основного проекта (опционально)

- FileManagerEventChangeFolderPath ($oldPath, $newPath) - выбрысывается при изменении пути и/или названия папки
- FileManagerEventChangeFilePath ($oldPath, $newPath) - выбрысывается при изменении пути и/или названия файла

## Notices
- для preview изображений обязательно должен существовать storage с ролью "cache"

## Описание config файла
```
'guard' => 'user' // Laravel Auth User guard
'storages' => [ // Список storage, которые будут отображаться в filemanager
    [
        'disk_name' => 'file-manager-public',    // Название storage
        'folder_name' => 'Public Folder',       // Название папки в filemanager
        'folder_feather' => 'folder',           // feather-icon папки в filemanager
        'default' => true,                      // Дефолтный storage, который открывается при загрузке filemanager
        'role' => 'public',                     // Роли storage (public | private | trash | cache)
        'permissions' => [                      // Настройка, что разрешено делать в данном storage
            'upload' => true,                   // загрузка / вставка файлов
            'create_folder' => true,            // создание папок
            'delete' => true,                   // удаление
            'rename' => true,                   // переименование
            'copy' => true,                     // копирование
            'cut' => true                       // вырезание
        ]
    ],
],
// события, для работы с моделями
'events' => [         
    // изменение пути папки, метод принимает 2 аргумента ($oldPath, $newPath)
    'changeFolderPath' => [ 
        // класс модели => название метода
        [\App\Models\Admin::class => 'changeImageFolder'] 
    ],

    // изменение пути файла, метод принимает 2 аргумента ($oldPath, $newPath)
    'changeFilePath' => [ 
        [\App\Models\Admin::class => 'changeImageFile']
    ],

    // проверка использования файла, выполняется перед удалением, принимает 1 агрумент ($filePath)
    // если файл используется, возвращает строку вида "Admin #228"
    // если файл не используется, возвращает false
    'checkUsedImage' => [
        [\App\Models\Admin::class => 'checkUsedImage']
    ]
]
```
    
#### Пример кода для changeImageFolder
```
public static function changeImageFolder($pathOld, $pathNew) {
    if ($result = self::where('avatar', 'like', $pathOld.'/%')->get()) {
        foreach ($result as $item) {
            $item->update(['avatar' => str_replace($pathOld, $pathNew, $item->avatar)]);
        }
    }
}
```

#### Пример кода для changeImageFile
```
public static function changeImageFile($pathOld, $pathNew) {
    self::where(['avatar' => $pathOld])->update(['avatar' => $pathNew]);
}
```

#### Пример кода для checkUsedImage
```
public static function checkUsedImage($path) {
    if ($item = self::where(['avatar' => $path])->first()) {
        return "Admin #".$item->getKey();
    }
    return false;
}
```

## Доделки на будущее
- blade шаблон
- локализация(переводы)
- свой middleware для выходных данных (config:array class => method, который принимает и возращает выходной массив данных)
- убрать guard из config в Request
