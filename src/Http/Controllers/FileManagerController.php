<?php

namespace FileManager\Http\Controllers;


use App\Http\Controllers\admin\HelperController;
use FileManager\Events\FileManagerEventChangeFilePath;
use FileManager\Events\FileManagerEventChangeFolderPath;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class FileManagerController extends Controller {
    public $guard = '';
    public $guard_primary_name = "";
    public $guard_user_id = 0;

    protected $storages = [];
    protected $events = [];

    private $config_option_disk_name = 'disk_name';
    private $config_option_folder_name = 'folder_name';
    private $config_option_feather = 'folder_feather';
    private $config_option_role = 'role';
    private $config_option_permissions = 'permissions';

    protected $current_folder = "";
    protected $current_file = "";

    protected $main_disc = "";
    protected $cache_disc = "";
    protected $trash_disc = "";

    public function __construct(Request $request){
        $this->guard = config('file-manager.guard');
        $this->events = config('file-manager.events');
        if ($this->guard) {
            $this->guard_primary_name = app(config('auth.providers')[$this->guard]['model'])->getKeyName();
        } else {
            $this->guard_primary_name = app(config('auth.providers')['model'])->getKeyName();
        }

        // storages init
        foreach (config('file-manager.storages') as $storage) {
            if (!$this->main_disc && $storage[$this->config_option_role] == 'public') {
                $this->main_disc = $storage[$this->config_option_disk_name];
            } else if (!$this->cache_disc && $storage[$this->config_option_role] == 'cache') {
                $this->cache_disc = $storage[$this->config_option_disk_name];
            } else if (!$this->trash_disc && $storage[$this->config_option_role] == 'trash') {
                $this->trash_disc = $storage[$this->config_option_disk_name];
            }

            $this->storages[$storage[$this->config_option_disk_name]] = $storage;
        }

        $this->middleware(function ($request, $next) {
            if(!Auth::guard($this->guard)->check()) return $request->isMethod('post') ? response()->json('Expire session', 401) : redirect(route('user.login'));
            $guard_user = Auth::guard($this->guard)->user()->toArray();
            $this->guard_user_id = $guard_user[$this->guard_primary_name];

            return $next($request);
        });

        if($request->has('current_folder') && $request->isMethod('post')){
            $this->current_folder = $request->post('current_folder');
        }
        if($request->has('current_file') && $request->isMethod('post')){
            $this->current_file = $request->post('current_file');

            if (!$request->post('disc') && Storage::exists($this->current_file)) {
                $path_arr = explode('/', $this->current_file);
                $disk = array_shift($path_arr);
                $request->request->add(['disc'=> $disk]);

                $this->current_file = implode('/', $path_arr);
            } else if (!$request->post('disc') && Storage::disk($this->main_disc)->exists($this->current_file)) {
                $request->request->add(['disc'=> $this->main_disc]);
            }

            if($request->has('disc') && $request->post('disc') && Storage::disk($request->post('disc'))->exists($this->current_file)){
                $path_arr = explode('/', $this->current_file);
                array_pop($path_arr);
                $this->current_folder = implode('/', $path_arr);
            }
        }
    }

    public function getDefaultDisk() {
        $firstDisk = "";
        foreach ($this->storages as $storage) {
            if ($storage['default']) {
                return $storage[$this->config_option_disk_name];
            }

            if (!$firstDisk) {
                $firstDisk = $storage[$this->config_option_disk_name];
            }
        }

        return $firstDisk;
    }

    /**
     *
     * Проверяет, создана ли папка для приватного диска
     *
     */
    public function checkPrivateFolder($disk) {
        $storage = $this->storages[$disk];
        if (isset($storage[$this->config_option_role]) && $storage[$this->config_option_role] == 'private') {
            if (!Storage::disk($disk)->exists($this->guard_user_id)) {
                Storage::disk($disk)->makeDirectory($this->guard_user_id);
            }

            return true;
        }

        return false;
    }

    public function makeSidebar() {
        $arr = [];
        foreach ($this->storages as $storage) {
            $arr[] = [
                'disk' => $storage[$this->config_option_disk_name],
                'folder' => $storage[$this->config_option_folder_name],
                'feather' => isset($storage[$this->config_option_feather]) ? $storage[$this->config_option_feather] : ""
            ];
        }

        return $arr;
    }

    public function init(Request $request, $disk = false, $folder = false){
        $response = [];

        if(!$disk){
            if(!is_null($request) && $request->has('disc') && $request->post('disc')) {
                $disk = $request->post('disc');
            } else {
                $disk = $this->getDefaultDisk();
            }
        }

        if ($folder) {
            $this->current_folder = $folder;
        }

        // проверка, существует ли личная папка
        $is_private_disk = $this->checkPrivateFolder($disk);

        $access = true;
        if ($is_private_disk) {
            if (!$this->current_folder) {
                // не даем выйти в корень приватного storage
                $this->current_folder = $this->guard_user_id;
                $response['current_folder'] = $this->current_folder;
            }

            // проверка прав на приватную папку
            $user_folder = explode('/', $this->current_folder)[0];
            if ($user_folder != $this->guard_user_id) {
                $access = false;
            }
        }

        $folders = Storage::disk($disk)->exists($this->current_folder) ? Storage::disk($disk)->directories($this->current_folder) : Storage::disk($disk)->directories();
        $response['folders'] = array();
        if($access && $folders){
            foreach ($folders as $folder){
                $response['folders'][] = self::getFolderInfo($folder, $disk);
            }
        }
        $files = Storage::disk($disk)->exists($this->current_folder) ? Storage::disk($disk)->files($this->current_folder) : Storage::disk($disk)->files();
        $response['files'] = array();
        if($access && $files){
            foreach ($files as $file){
                $response['files'][] = self::getFileInfo($file, $disk);
            }
        }

        $name_arr = explode("/", $this->current_folder);
        $parent = array_pop($name_arr);
        $response['parent_folder'] = implode('/', $name_arr);
        $response['parent_folder_name'] = $parent ? $parent : "Home";
        $response['selected_file'] = $this->current_file;
        $response['disc'] = $disk;
        $response['sidebar'] = $this->makeSidebar();
        $response['permissions'] = $this->storages[$disk][$this->config_option_permissions];
        return $response;
    }

    protected function getFolderInfo($folder, $disc){
        $response = array();
        $pathinfo = pathinfo(realpath(Storage::disk($disc)->getDriver()->getAdapter()->getPathPrefix() . $folder));
        $stat = stat(realpath(Storage::disk($disc)->getDriver()->getAdapter()->getPathPrefix() . $folder));
        $response['el_id'] = md5($pathinfo['filename']);
        $response['is_folder'] = true;
        $response['name'] = $pathinfo['filename'];
        $response['storage_name'] = $folder;
        $response['total_folders'] = count(Storage::disk($disc)->directories($folder));
        $response['total_files'] = count(Storage::disk($disc)->files($folder));
        $response['total'] = $response['total_folders'] + $response['total_files'] . ' items';
        $response['last_change'] = date('d F Y', $stat['mtime']) . " at " . date('H:i', $stat['mtime']);
        $response['created_at'] = date('d F Y', $stat['ctime']) . " at " . date('H:i', $stat['ctime']);
        return $response;
    }

    protected function getFileInfo($file, $disc){
        $response = array();
        $pathinfo = pathinfo(realpath(Storage::disk($disc)->getDriver()->getAdapter()->getPathPrefix() . $file));
        $stat = stat(realpath(Storage::disk($disc)->getDriver()->getAdapter()->getPathPrefix() . $file));
        $response['el_id'] = md5($pathinfo['basename']);
        $response['is_folder'] = false;
        $response['name'] = $pathinfo['filename'];
        $response['storage_name'] = $file;
        $response['size'] = self::getPrettySize(Storage::disk($disc)->size($file));
        $response['ext'] = isset($pathinfo['extension']) ? $pathinfo['extension'] : "?";
        $response['last_change'] = date('d F Y', $stat['mtime']) . " at " . date('H:i', $stat['mtime']);
        $response['created_at'] = date('d F Y', $stat['ctime']) . " at " . date('H:i', $stat['ctime']);
        $response['public_url'] = asset("/{$disc}/{$file}");
        $response['preview'] = self::getPreview($file, $disc);
        $response['preview_small'] = self::getPreview($file, $disc, 100, 100);
        $response['preview_big'] = self::getPreview($file, $disc, 400, 400);
        $response['image_size'] = self::getImageSize($file, $disc);
        return $response;
    }

    protected function getPrettySize($size){
        if($size<=1024) return $size.' bytes';
        else if($size<=1024*1024) return round($size/(1024),2).' Kb';
        else if($size<=1024*1024*1024) return round($size/(1024*1024),2).' Mb';
        else if($size<=1024*1024*1024*1024) return round($size/(1024*1024*1024),2).' Gb';
        else if($size<=1024*1024*1024*1024*1024) return round($size/(1024*1024*1024*1024),2).' Tb'; //:)))
        else return round($size/(1024*1024*1024*1024*1024),2).' Pb'; // ;-)
    }

    public function getPreview($file, $disc, $needed_width = 200, $needed_height = 200, $quality = 85, $webp = false){
        try{

            $find = false;
            $full_path = false;
            if (Storage::disk($disc)->exists($file)) {
                $find = true;
            } else if (Storage::exists($file)) {
                $find = true;
                $full_path = true;
            }

            if($find){
                if ($full_path) {
                    $file_path = "/{$file}";
                } else {
                    $file_path = "/{$disc}/{$file}";
                }
                
                $file_full_path = storage_path('app') . $file_path;
                $path_info = pathinfo($file_full_path);
                $extension = strtolower($path_info['extension']);
                if($webp){
                    $extension = "webp";
                }elseif ($extension == "webp" && !$webp){
                    $extension = "png";
                }
                $mime_type = mime_content_type($file_full_path);
                $post_fix = md5($file_path);
                if(!$needed_height && !$needed_width){
                    $cache_file_path = "/orig/" . substr($post_fix, 0, 3) . "/" . substr($post_fix, 3, 3) . "/" . substr($post_fix, 6, 3) . "/" . $post_fix;
                }else{
                    $cache_file_path = "/{$needed_width}_{$needed_height}/" . substr($post_fix, 0, 3) . "/" . substr($post_fix, 3, 3) . "/" . substr($post_fix, 6, 3) . "/" . $post_fix;
                }
                $cache_file_path_full = $cache_file_path . "/" . $path_info['filename'] . ".{$extension}";
                $allowed = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/webp',
                    'image/svg',
                    'image/svg+xml',
                );

                if(!in_array($mime_type, $allowed)) return "";
                if($mime_type === 'image/svg' || $mime_type === 'image/svg+xml') return asset("{$file_path}");
                if (!$this->cache_disc) {
                    return "";
                }

                if(Storage::disk($this->cache_disc)->exists($cache_file_path_full)) return asset("/{$this->cache_disc}{$cache_file_path_full}");
                Storage::disk($this->cache_disc)->makeDirectory($cache_file_path);

                list($width_orig, $height_orig) = getimagesize($file_full_path);
                $ratio_orig = $width_orig/$height_orig;
                if(!$needed_height && !$needed_width){
                    $needed_width = $width_orig;
                    $needed_height = $height_orig;
                }
                if ($needed_width/$needed_height > $ratio_orig) {
                    $needed_width = $needed_height*$ratio_orig;
                } else {
                    $needed_height = $needed_width/$ratio_orig;
                }
                if ($mime_type == 'image/gif') {
                    $image = imagecreatefromgif($file_full_path);
                } elseif ($mime_type == 'image/png') {
                    $image = imagecreatefrompng($file_full_path);
                } elseif ($mime_type == 'image/jpeg') {
                    $image = imagecreatefromjpeg($file_full_path);
                }else{
                    return "";
                }
                $height = ceil($height_orig * $needed_width / $width_orig);
                $image_p = imagecreatetruecolor($needed_width, $height);
                if ($mime_type == 'image/png') {
                    imagealphablending($image_p, false);
                    imagesavealpha($image_p, true);
                    $background = imagecolorallocatealpha($image_p, 255, 255, 255, 127);
                    imagecolortransparent($image_p, $background);
                } else {
                    $background = imagecolorallocate($image_p, 255, 255, 255);
                }
                imagefilledrectangle($image_p, 0, 0, $needed_width, $height, $background);
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $needed_width, $height, $width_orig, $height_orig);
                if (is_resource($image_p)) {
                    if (($extension == 'jpeg' || $extension == 'jpg') && function_exists("imagejpeg")) {
                        imagejpeg($image_p, storage_path('app') . "/{$this->cache_disc}{$cache_file_path_full}", $quality);
                    } elseif ($extension == 'png' && function_exists("imagepng")) {
                        imagepng($image_p, storage_path('app') . "/{$this->cache_disc}{$cache_file_path_full}");
                    } elseif ($extension == 'gif' && function_exists("imagegif")) {
                        imagegif($image_p, storage_path('app') . "/{$this->cache_disc}{$cache_file_path_full}");
                    }elseif ($extension == 'webp' && function_exists("imagewebp")) {
                        imagewebp($image_p, storage_path('app') . "/{$this->cache_disc}{$cache_file_path_full}", $quality);
                    }else{
                        imagedestroy($image_p);
                        return "";
                    }
                    imagedestroy($image_p);
                }
                return asset("/{$this->cache_disc}{$cache_file_path_full}");
            }else{
                return "";
            }
        }catch (\Exception $exception){
            return "";
        }

        return "";
    }

    protected function getImageSize($file, $disc){
        try{
            if(Storage::disk($disc)->exists($file)){
                $file_path = "/{$disc}/{$file}";
                $file_full_path = public_path() . $file_path;
                $mime_type = mime_content_type($file_full_path);
                $allowed = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/webp',
                );
                if(!in_array($mime_type, $allowed)) return "";
                list($width_orig, $height_orig) = getimagesize($file_full_path);
                return "{$width_orig}px×{$height_orig}px";
            }
            return "";
        }catch (\Exception $exception){
            return "";
        }
    }

    protected function isImage($file, $disc){
        try{
            if(Storage::disk($disc)->exists($file)){
                $file_path = "/{$disc}/{$file}";
                $file_full_path = public_path() . $file_path;
                $mime_type = mime_content_type($file_full_path);
                $allowed = array(
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/webp',
                );
                if(!in_array($mime_type, $allowed)) return false;
                return true;
            }
            return false;
        }catch (\Exception $exception){
            return false;
        }
    }

    public function create(Request $request, $i = 0){
        $folder_name = "NewFolder";
        if ($request->post('folder_name')) {
            $folder_name = $request->post('folder_name');
        }

        $disc = $request->post('disc');
        $new_folder = $this->current_folder ? $this->current_folder . '/'.$folder_name : $folder_name;
        if($i) $new_folder = $new_folder . "({$i})";
        if(Storage::disk($disc)->exists($new_folder)){
            $i++;
            return self::create($request, $i);
        }else{
            return array('result' => Storage::disk($disc)->makeDirectory($new_folder));
        }
    }

    public function checkDeleteFiles(Request $request) {
        $disc = $request->post('disc');
        if($request->has('selected') && $request->isMethod('post') && is_array($request->post('selected')) && $selected_items = array_filter($request->post('selected'))){
            foreach ($selected_items as $selected_item){
                if(Storage::disk($disc)->exists($selected_item)){
                    if(is_dir(realpath(Storage::disk($disc)->getDriver()->getAdapter()->getPathPrefix() . $selected_item))){
                        if($files = Storage::disk($disc)->allFiles($selected_item)){
                            foreach ($files as $file){
                                if ($result = $this->searchUsingFile($disc.'/'.$file)) {
                                    return ['object' => $result, 'file' => $file];
                                }
                            }
                        }
                    } else {
                        if ($result = $this->searchUsingFile($disc.'/'.$selected_item)) {
                            return ['object' => $result, 'file' => $selected_item];
                        }
                    }
                }
            }
        }

    }

    public function delete(Request $request){
        $disc = $request->post('disc');
        if($request->has('selected') && $request->isMethod('post') && is_array($request->post('selected')) && $selected_items = array_filter($request->post('selected'))){
            foreach ($selected_items as $selected_item){
                if(Storage::disk($disc)->exists($selected_item)){
                    if(is_dir(realpath(Storage::disk($disc)->getDriver()->getAdapter()->getPathPrefix() . $selected_item))){
                        if($files = Storage::disk($disc)->allFiles($selected_item)){
                            foreach ($files as $file){
                                self::moveToTrash($file, $disc);
                            }
                        }
                        Storage::disk($disc)->deleteDirectory($selected_item);
                    }else{
                        self::moveToTrash($selected_item, $disc);
                    }
                }
            }
        }

        return self::init($request, $disc, $request->post('current'));
    }

    protected function moveToTrash($file, $disc, $i = 0){
        if ($this->storages[$disc][$this->config_option_role] != 'trash' && $this->trash_disc) {
            if(Storage::disk($disc)->exists($file)){
                $path_info = pathinfo(implode('/', array(Storage::disk($disc)->getDriver()->getAdapter()->getPathPrefix(), $file)));
                $file_name = $path_info['basename'];
                if ($i) {
                    if ($i) $file_name = $path_info['filename'] . "({$i})." . $path_info['extension'];
                }
                if (Storage::disk($this->trash_disc)->exists($file_name)) {
                    $i++;
                    return self::moveToTrash($file, $disc, $i);
                }
                Storage::move("/{$disc}/{$file}", "/{$this->trash_disc}/{$file_name}");
            }
        } else {
            if(Storage::disk($disc)->exists($file)){
                Storage::disk($disc)->delete($file);
            }
        }

        return true;
    }

    public function upload(Request $request){
        try{
            $errors = array();
            $allowed = array(
                'text/plain',
                'image/png',
                'image/jpeg',
                'image/gif',
                'image/bmp',
                'image/tiff',
                'image/webp',
                'image/svg+xml',
                'image/svg',
                'application/zip',
                '"application/zip"',
                'application/x-zip',
                '"application/x-zip"',
                'application/x-zip-compressed',
                '"application/x-zip-compressed"',
                'application/rar',
                '"application/rar"',
                'application/x-rar',
                '"application/x-rar"',
                'application/x-rar-compressed',
                '"application/x-rar-compressed"',
                'application/octet-stream',
                '"application/octet-stream"',
                'audio/mpeg',
                'video/quicktime',
                'application/pdf',
                'video/mp4'
            );

            $disk = $request->post('disk');

            if($request->has('drop_folder') && $request->post('drop_folder') && Storage::disk($disk)->exists($request->post('drop_folder'))) {
                foreach ($request->files as $file){
                    if(in_array($file->getMimeType(), $allowed)){
                        self::putFiles($disk, $file, $request->post('drop_folder'));
                    }else{
                        $errors[] = "File {$file->getClientOriginalName()} is not allowed";
                    }
                }
            }elseif($this->current_folder && Storage::disk($disk)->exists($this->current_folder)){
                foreach ($request->files as $file){
                    if(in_array($file->getMimeType(), $allowed)){
                        self::putFiles($disk, $file, $this->current_folder);
                    }else{
                        $errors[] = "File {$file->getClientOriginalName()} is not allowed";
                    }
                }
            }else{
                foreach ($request->files as $file){
                    if(in_array($file->getMimeType(), $allowed)){
                        self::putFiles($disk, $file, false);
                    }else{
                        $errors[] = "File {$file->getClientOriginalName()} is not allowed";
                    }
                }
            }

            return self::init($request, $disk) + compact('errors');
        }catch (\Exception $exception){
            $errors[] = $exception->getMessage();
            return self::init($request, $disk) + compact('errors');
        }
    }

    protected function putFiles($disk, $file, $folder, $i = 0){
        try{
            $file_name_arr = explode('.', $file->getClientOriginalName());
            $file_pop = array_pop($file_name_arr);
            $ext = $file_pop;
            $file_name = implode('.', $file_name_arr) .".{$ext}";
            if($i){
                if($i) $file_name = implode('.', $file_name_arr) . "({$i}).{$ext}";
            }
            if($folder && Storage::disk($disk)->exists(implode('/', array($folder, $file_name)))){
                $i++;
                return self::putFiles($disk, $file, $folder, $i);
            }elseif (!$folder && Storage::disk($disk)->exists($file_name)){
                $i++;
                return self::putFiles($disk, $file, $folder, $i);
            }
            if($folder){
                $file = Storage::disk($disk)->putFileAs(
                    $folder,
                    $file,
                    $file_name
                );
            }else{
                $file = Storage::disk($disk)->putFileAs(
                    "/",
                    $file,
                    $file_name
                );
            }
            return $file;
        }catch (\Exception $exception){
            return "";
        }
    }

    public function search(Request $request){
        $search = ($request->has('search') && $request->post('search')) ? strtolower(trim($request->post('search'))) : "";
        $disk = $request->post('disc');

        if(!$search) return self::init($request, $disk);
        $response['files'] = array();
        $all_files = Storage::disk($disk)->allFiles();
        $search_arr = array();
        for ($i = 1; $i <= count($all_files); $i++){
            $search_arr[] = $search;
        }

        $files = $search ? array_filter(array_map('self::arraySearch', $all_files, $search_arr)) : $all_files;
        if($files){
            foreach ($files as $file){
                $response['files'][] = self::getFileInfo($file, $disk);
            }
        }
        $response['folders'] = array();
        $all_folders = Storage::disk($disk)->allDirectories();
        $folders = $search ? array_filter(array_map('self::arraySearch', $all_folders, $search_arr)) : $all_folders;
        if($folders){
            foreach ($folders as $folder){
                $response['folders'][] = self::getFolderInfo($folder, $disk);
            }
        }
        $response['parent_folder'] = "";
        $response['parent_folder_name'] = "Home";
        $response['selected_file'] = "";
        $response['disc'] = $disk;
        return $response;
    }

    public function arraySearch($arr_val, $search){
        $arr_val_exp = explode('/', $arr_val);
        $search_arr_val = end($arr_val_exp);

        if($search && strpos($search_arr_val, $search) !== false){
            return $arr_val;
        }
    }

    public function insert(Request $request) {
        $from_disk = $request->post('from_disk');
        $to_disk = $request->post('to_disk');
        $to_folder = $request->post('to_folder');
        $selected = $request->post('selected');
        $action = $request->post('action');

        foreach ($selected as $selected_item) {
            $new_name_arr = explode('/', $selected_item);
            $file_name = array_pop($new_name_arr);
            $real_path = realpath(Storage::disk($from_disk)->getDriver()->getAdapter()->getPathPrefix() . $selected_item);
            $to_path = $to_folder . "/" . $file_name;

            $new_index = 0;
            $to_path_original = $to_path;
            while (true) {
                if (!Storage::disk($to_disk)->exists($to_path)) {
                    break;
                }

                $new_index++;
                $to_path = $this->renameDuplicate($to_disk, $to_path_original, $new_index);
            }

            if (is_dir($real_path)) {
                // реализовать приход по folder внутри метода copy
                if ($action == 'copy') {
                    $this->copyFolder($from_disk, $selected_item, $to_disk, $to_path);
                } else if ($action == 'cut') {
                    // переносим
                    $this->moveFolder($from_disk, $selected_item, $to_disk, $to_path);
                }
            } else {
                if ($action == 'copy') {
                    $this->copyFile($from_disk, $selected_item, $to_disk, $to_path);
                } else if ($action == 'cut') {
                    $this->moveFile($from_disk, $selected_item, $to_disk, $to_path);
                }
            }
        }

        return self::init($request, $to_disk, $to_folder);
    }

    public function rename(Request $request){
        $disc = $request->post('disc');
        if($request->has('storage_obj') && $request->post('storage_obj') && Storage::disk($disc)->exists($request->post('storage_obj'))) {
            if($request->has('storage_name') && $request->post('storage_name') && is_dir(realpath(Storage::disk($disc)->getDriver()->getAdapter()->getPathPrefix() . $request->post('storage_obj')))){
                $new_name_arr = explode('/', $request->post('storage_obj'));
                array_pop($new_name_arr);
                $new_name_arr[] = $request->post('storage_name');

                Storage::disk($disc)->move($request->post('storage_obj'), implode('/', $new_name_arr));
                $this->changeUsingFolderPath($disc."/".$request->post('storage_obj'), $disc."/".implode('/', $new_name_arr));
            }elseif($request->has('storage_name') && $request->post('storage_name') && is_file(realpath(Storage::disk($disc)->getDriver()->getAdapter()->getPathPrefix() . $request->post('storage_obj')))){
                $new_name_arr = explode('/', $request->post('storage_obj'));
                $file_name = array_pop($new_name_arr);
                $ext_arr = explode('.', $file_name);
                $ext = array_pop($ext_arr);
                $new_name_arr[] = $request->post('storage_name') . ".{$ext}";

                Storage::disk($disc)->move($request->post('storage_obj'), implode('/', $new_name_arr));
                $this->changeUsingFilePath($disc."/".$request->post('storage_obj'), $disc."/".implode('/', $new_name_arr));
            }
        }
        return self::init($request, $disc);
    }

    private function moveFile($from_disk, $from_path, $to_disk, $to_path) {
        if ($from_disk == $to_disk) {
            Storage::disk($from_disk)->move($from_path, $to_path);
        } else {
            Storage::move($from_disk.'/'.$from_path, $to_disk.'/'.$to_path);
        }

        $this->changeUsingFilePath($from_disk."/".$from_path, $to_disk."/".$to_path);
    }

    private function copyFile($from_disk, $from_path, $to_disk, $to_path) {
        if ($from_disk == $to_disk) {
            Storage::disk($to_disk)->copy($from_path, $to_path);
        } else {
            Storage::copy($from_disk.'/'.$from_path, $to_disk.'/'.$to_path);
        }
    }

    private function copyFolder($from_disk, $from_path, $to_disk, $to_path) {
        $real_path = realpath(Storage::disk($from_disk)->getDriver()->getAdapter()->getPathPrefix() . $from_path);
        if (is_dir($real_path)) {
            // копировать файлы
            if($files = Storage::disk($from_disk)->allFiles($from_path)) {
                foreach ($files as $file) {
                    $to_file = str_replace($from_path, $to_path, $file);
                    $this->copyFolder($from_disk, $file, $to_disk, $to_file);
                }
            }

        } else {
            $this->copyFile($from_disk, $from_path, $to_disk, $to_path);
        }
    }

    private function moveFolder($from_disk, $from_path, $to_disk, $to_path) {
        $real_path = realpath(Storage::disk($from_disk)->getDriver()->getAdapter()->getPathPrefix() . $from_path);
        if (is_dir($real_path)) {
            // копировать файлы
            if($files = Storage::disk($from_disk)->allFiles($from_path)) {
                foreach ($files as $file) {
                    $to_file = str_replace($from_path, $to_path, $file);
                    $this->moveFolder($from_disk, $file, $to_disk, $to_file);
                }
            }

            // удаляем
            if (Storage::disk($from_disk)->exists($from_path)) {
                Storage::disk($from_disk)->deleteDirectory($from_path);
            }
        } else {
            $this->moveFile($from_disk, $from_path, $to_disk, $to_path);
        }
    }

    private function renameDuplicate($disk, $path, $index) {
        $real_path = realpath(Storage::disk($disk)->getDriver()->getAdapter()->getPathPrefix() . $path);
        if (is_file($real_path)) {
            $infoPath = pathinfo($real_path);
            $extension = $infoPath['extension'];

            $path = str_replace('.'.$extension, '('.$index.').'.$extension, $path);
        } else {
            $path .= "(".$index.")";
        }

        return $path;
    }

    public function getCachedImage(Request $request){
        return self::getPreview($request->post('image'), 'file-manager', 200, 200);
    }

    public function changeUsingFolderPath($oldPath, $newPath) {
        if (isset($this->events['changeFolderPath']) && $this->events['changeFolderPath']) {
            foreach ($this->events['changeFolderPath'] as $arr) {
                foreach ($arr as $model => $method) {
                    $model_obj = app($model);
                    $model_obj->$method($oldPath, $newPath);
                }
            }
        }

        event(new FileManagerEventChangeFolderPath($oldPath, $newPath));
    }

    public function changeUsingFilePath($oldPath, $newPath) {
        if (isset($this->events['changeFilePath']) && $this->events['changeFilePath']) {
            foreach ($this->events['changeFilePath'] as $arr) {
                foreach ($arr as $model => $method) {
                    $model_obj = app($model);
                    $model_obj->$method($oldPath, $newPath);
                }
            }
        }

        event(new FileManagerEventChangeFilePath($oldPath, $newPath));
    }

    public function searchUsingFile($path) {
        if (isset($this->events['checkUsedImage']) && $this->events['checkUsedImage']) {
            foreach ($this->events['checkUsedImage'] as $arr) {
                foreach ($arr as $model => $method) {
                    $model_obj = app($model);
                    $result = $model_obj->$method($path);

                    if ($result) {
                        return $result;
                    }
                }
            }
        }

        return false;
    }

    public function clearDisk(Request $request) {
        $disk = $request->post('disk');
        if (!$disk) {
            return;
        }

        $directories = Storage::disk($disk)->directories();
        foreach ($directories as $selected_item){
            if($files = Storage::disk($disk)->allFiles($selected_item)){
                foreach ($files as $file){
                    self::moveToTrash($file, $disk);
                }
            }
            Storage::disk($disk)->deleteDirectory($selected_item);
        }

        $files = Storage::disk($disk)->files();
        foreach ($files as $file){
            self::moveToTrash($file, $disk);
        }
    }
}
