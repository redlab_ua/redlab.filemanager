<?php

namespace FileManager;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use FileManager\FileManager;

class FileManagerServiceProvider extends BaseServiceProvider {
    public function boot() {
        $this->publishes([
            __DIR__ . '/../configs/file-manager.php' => config_path('file-manager.php'),
        ]);

        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
    }

    public function register() {
        /*$this->mergeConfigFrom(
            __DIR__ . '/../configs/file-manager.php', 'file-manager'
        );*/

        $this->app->bind('FileManager',function(){
            return new FileManager();
        });
    }
}
