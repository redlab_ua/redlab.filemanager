<?php

use Illuminate\Support\Facades\Route;
use FileManager\Http\Controllers\FileManagerController;

if (config('file-manager.guard')) {
    Route::group(['middleware' => ['web']], function () {
        Route::group(array('prefix' => '/'.config('file-manager.guard'),'as'=> config('file-manager.guard').'.'), function () {
            Route::group(array('prefix' => '/file-manager', 'as' => 'file-manager.'), function () {
                Route::post('/init', [FileManagerController::class, 'init']);
                Route::post('/create', [FileManagerController::class, 'create']);
                Route::post('/delete', [FileManagerController::class, 'delete']);
                Route::post('/upload', [FileManagerController::class, 'upload']);
                Route::post('/search', [FileManagerController::class, 'search']);
                Route::post('/rename', [FileManagerController::class, 'rename']);
                Route::post('/insert', [FileManagerController::class, 'insert']);
                Route::post('/get-cached-image', [FileManagerController::class, 'getCachedImage']);
                Route::post('/check-delete-files', [FileManagerController::class, 'checkDeleteFiles']);
                Route::post('/clear-disk', [FileManagerController::class, 'clearDisk']);
            });
        });
    });
} else {
    Route::group(['middleware' => ['web']], function () {
            Route::group(array('prefix' => '/file-manager', 'as' => 'file-manager.'), function () {
                Route::post('/init', [FileManagerController::class, 'init']);
                Route::post('/create', [FileManagerController::class, 'create']);
                Route::post('/delete', [FileManagerController::class, 'delete']);
                Route::post('/upload', [FileManagerController::class, 'upload']);
                Route::post('/search', [FileManagerController::class, 'search']);
                Route::post('/rename', [FileManagerController::class, 'rename']);
                Route::post('/insert', [FileManagerController::class, 'insert']);
                Route::post('/get-cached-image', [FileManagerController::class, 'getCachedImage']);
                Route::post('/check-delete-files', [FileManagerController::class, 'checkDeleteFiles']);
                Route::post('/clear-disk', [FileManagerController::class, 'clearDisk']);
            });
    });
}


