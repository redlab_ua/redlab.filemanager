import Vue from 'vue'
import Swal from 'sweetalert2';
import Notifications from 'vue-notification';
Vue.use(Notifications);
let modal = {
    confirm: function (title = '', description = '', func = () =>{}) {
        Swal.fire({
            title: title,
            text: description,
            icon: "question",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            cancelButtonClass: 'btn',
            confirmButtonClass: 'btn',
        }).then((result) => {
            if (result.value) {
                func()
            }
        });
    },
    warning: function (title = '', description = '') {
        Vue.notify({
            group: 'foo',
            type: 'bg-warning',
            position: 'bottom right',
            title: title,
            text: description
        });
    },
    error: function (title = '', description = '') {
        Vue.notify({
            group: 'foo',
            type: 'bg-danger',
            position: 'bottom right',
            title: title,
            text: description
        });
    },
    success: function (title = '', description = '') {
        Vue.notify({
            group: 'foo',
            type: 'bg-success',
            position: 'bottom left',
            title: title,
            text: description
        });
    },
};
export default modal;