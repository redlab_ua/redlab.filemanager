window.Vue = require('vue');

import feather from 'feather-icons';
window.Vue.prototype.$url_prefix = '';
window.Vue.prototype.$feather = feather;

import FileManagerIndex from './components/FileManagerIndex';
import FileManagerWrapper from './components/FileManagerWrapper';

window.Vue.component('file-manager', FileManagerIndex);
window.Vue.component('file-manager-wrapper', FileManagerWrapper);

import PerfectScrollbar from 'vue2-perfect-scrollbar'
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css'
window.Vue.use(PerfectScrollbar);

import modal from './modal.js';
window.Vue.prototype.$modal = modal;

